class User < ActiveRecord::Base
  include RocketPants::Cacheable

  attr_accessible :about, :company_name, :company_url, :email, :first_name, :google_plus, :last_name, :linked_in, :phone, :skype, :zip_code

  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  URL_REGEXP = URI::regexp(%w(http https))

  validates :email, :format => { :with => EMAIL_REGEXP, :on => :create }
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :company_url, :format => { :with => URL_REGEXP }
  validates :google_plus, :format => { :with => URL_REGEXP }
  validates :linked_in, :format => { :with => URL_REGEXP }

  has_and_belongs_to_many :needs
  has_and_belongs_to_many :offerings
  has_many :meetings
  has_many :matches

  def upcoming_meetings
    meetings.upcoming.all
  end

  def past_meetings
    meetings.past.all
  end
end
