require 'spec_helper'

describe User do
  before :all do
    @mike = FactoryGirl.create :user_mike_jones
    @sally = FactoryGirl.create :user_sally_smith
    @bob = FactoryGirl.create :user_bob_poole
  end
  
  it 'passes a sanity check' do
    @bob.first_name.should  eql('Bob')
    @bob.matches.map(&:id).should include(@mike.id, @sally.id)
  end

  context 'validations' do
    context 'malformed email addresses' do
      it 'disallows no tld' do
        lambda { FactoryGirl.create :user, email: 'no@tld' }.should raise_error(ActiveRecord::RecordInvalid)
      end

      it 'disallows no @' do
        lambda { FactoryGirl.create :user, email: 'no-at-tld.com' }.should raise_error(ActiveRecord::RecordInvalid)
      end

      it 'disallows no username' do
        lambda { FactoryGirl.create :user, email: '@nousername.com' }.should raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context 'malformed urls' do
      it 'disallows no schema' do
        lambda { FactoryGirl.create :user, company_url: 'google.com' }.should raise_error(ActiveRecord::RecordInvalid)
      end
    end

  end
end
