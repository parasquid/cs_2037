require 'spec_helper'

describe UsersController do
  default_version 1

  before :all do
    @mike = FactoryGirl.create :user_mike_jones
    @sally = FactoryGirl.create :user_sally_smith
    @bob = FactoryGirl.create :user_bob_poole
    @payload = { first_name: 'test', last_name: 'rspec', email: 'test@rspec.com',
      company_url: 'http://company.com', google_plus: 'http://google.com', linked_in: 'http://linked_in.com' }
  end

  it 'passes sanity check' do
    get :index
    response.should be_paginated_resource

    get :show, id: 3
    response.should be_singular_resource
  end

  it 'creates a user' do
    post :create, user: @payload
    response.should be_singular_resource
    response.decoded_body.response.first_name.should eql('test')
    response.decoded_body.response.last_name.should eql('rspec')
    response.decoded_body.response.email.should eql('test@rspec.com')
  end

  it 'updates a user' do
    put :update, id: @bob.id, user: { first_name: 'bob_modified' }
    response.should be_singular_resource
    response.decoded_body.response.first_name.should eql('bob_modified')    
  end

  it 'deletes a user' do
    count = User.count
    delete :destroy, id: @bob.id
    assert User.count == count - 1
    User.all.map(&:id).should_not include(@bob.id)
  end
end
