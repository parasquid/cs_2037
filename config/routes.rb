Parasquid2037::Application.routes.draw do
  resources :matches


  api version: 1 do
    resources :users do
      # change the route restrictions as needed
      resources :needs, only: [:get]
      resources :offerings, only: [:get]
      member do 
        get 'matches'
        scope '/meetings' do
          get 'upcoming'
          get 'past'
        end
      end
    end
  end
end
